<?php

/**
 * Implements hook_form
 */
function terrasoft_test_form_admin_form($form, &$form_state) {
  $form['mail'] = [
    '#type' => 'textfield',
    '#title' => t('EMail to:'),
    '#default_value' => !empty(variable_get('terrasoft_mail')) ? variable_get('terrasoft_mail') : '',
  ];
  $form['domains'] = [
    '#type' => 'fieldset',
    '#title' => t('Filter domains'),
    '#tree' => TRUE,
  ];
  for ($i = 1; $i <=3; ++$i) {
    $form['domains'][$i] = [
      '#type' => 'textfield',
      '#title' => t('Domain @num', ['@num' => $i]),
      '#default_value' => !empty(variable_get('terrasoft_domain')[$i]) ? variable_get('terrasoft_domain')[$i] : 'test'.$i,
    ];
  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];
  return $form;
}


/**
 * Implements hook_form_validate
 */
function terrasoft_test_form_admin_form_validate($form, &$form_state) {

  if(valid_email_address($form_state['values']['mail'])){
    form_set_error('mail', t('The email address appears to be invalid.'));
  }
}


/**
 * Implements hook_form_submit
 */
function terrasoft_test_form_admin_form_submit($form, &$form_state) {
  $states = $form_state['values'];
  variable_set('terrasoft_mail', $states['mail']);
  $domains = [];
  $domain = $states['domains'];
  for ($i = 1; $i <= 3; ++$i) {
    $domains[$i] = !empty($domain[$i]) ? $domain[$i] : '';
  }
  variable_set('terrasoft_domain', $domains);

}
