<?php

/**
 * Implements hook_mail()
 */
function terrasoft_test_form_mail($key, &$message, $params) {
  $site_email = variable_get('site_mail', '');

  if ($key == 'mail'){
      $message = array(
        'subject' => $site_email,
        'message' => t('You received a message on the website @site', array(
          '@site' => variable_get('site_name', '')
        )),
      );
  }
}
