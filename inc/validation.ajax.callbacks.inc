<?php

function domain_validator($form, &$form_state) {
  $container = '.domain';
  $arr_enabled = variable_get('terrasoft-domain', ['test1','test2', 'test3']);
  $commands = [];

  if ($form_state['values']['domain'] == '') {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'block']);
    $commands[] = ajax_command_invoke($container . ' input', 'addClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', 'Required');
  }
  elseif (array_search($form_state['values']['domain'], $arr_enabled)) {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'block']);
    $commands[] = ajax_command_invoke($container . ' input', 'addClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', t('Domain is busy'));
  }
  else {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'none']);
    $commands[] = ajax_command_invoke($container . ' input', 'removeClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', '');
  }

  $result = [
    '#type' => 'ajax',
    '#commands' => $commands,
  ];
  return $result;

}

function login_validator($form, &$form_state) {
  $container = '.login';

  $commands = [];
  if ($form_state['values']['login'] == '') {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'block']);
    $commands[] = ajax_command_invoke($container . ' input', 'addClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', 'Required');
  }
  else {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'none']);
    $commands[] = ajax_command_invoke($container . ' input', 'removeClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', '');
  }

  return [
    '#type' => 'ajax',
    '#commands' => $commands,
  ];
}

function country_validator($form, &$form_state) {
  $container = '.country';

  $commands = [];
  if ($form_state['values']['country'] == '0') {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'block']);
    $commands[] = ajax_command_invoke($container . ' input', 'addClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', 'Required');
  }
  else {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'none']);
    $commands[] = ajax_command_invoke($container . ' input', 'removeClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', '');
  }

  return [
    '#type' => 'ajax',
    '#commands' => $commands,
  ];
}

function code_validator($form, &$form_state) {
  $container = '.code';

  $commands = [];
  if ($form_state['values']['code'] == '') {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'block']);
    $commands[] = ajax_command_invoke($container . ' input', 'addClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', 'Required');
  }
  else {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'none']);
    $commands[] = ajax_command_invoke($container . ' input', 'removeClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', '');
  }

  return [
    '#type' => 'ajax',
    '#commands' => $commands,
  ];
}

function phone_validator($form, &$form_state) {
  $container = '.phone';

  $commands = [];
  if ($form_state['values']['phone'] == '') {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'block']);
    $commands[] = ajax_command_invoke($container . ' input', 'addClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', 'Phone Number is not valid');

  }
  else {
    $commands[] = ajax_command_css($container . '  .helper-text', ['display' => 'none']);
    $commands[] = ajax_command_invoke($container . ' input', 'removeClass', ['invalid']);
    $commands[] = ajax_command_html($container . '  .helper-text', '');
  }


  return [
    '#type' => 'ajax',
    '#commands' => $commands,
  ];
}

function form_validator($form, &$form_state) {

  $values = $form_state['values'];

  if ((array_search('', $values)) && (array_search('', $values)) != 'city') {
    $field = array_search('', $values);
    $field_class = '.' . $field;
    $selector=[
      'field'=>$field,
      'value'=>  $form_state['values'][$field],
    ];
    $commands[] = ajax_command_css($field_class . '  .helper-text', ['display' => 'block']);
    $commands[] = ajax_command_invoke($field_class . ' input', 'addClass', ['invalid']);
    $commands[] = ajax_command_settings($selector);
    $commands[] = ajax_command_html($field_class . '  .helper-text', 'Required');
  }
  else {
    $link = l('here', 'admin/config/test-form', [
      'attributes'=>[
        'class'=>['red-text']
      ]
    ]);
    $html = variable_get('teerasoft_mail') != ''?  '<h4 class="center-align terrasoft-head">Mail Send</h4>' : '<h4 class="center-align terrasoft-head">Mail not aviable</h4><p class="center-align domain-text">Click '.$link.'</p>';
    $commands[] = ajax_command_html('#form-background', $html);
  }
  return [
    '#type' => 'ajax',
    '#commands' => $commands,
  ];
}