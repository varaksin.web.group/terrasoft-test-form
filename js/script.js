
(function ($) {
    Drupal.behaviors.terrasoft_test_form = {
        attach: function (context, settings) {
            $('input#edit-domain, input#edit-login', context).characterCounter();
            $('.custom-phone-mask', context).mask('+38(099)999-99-99');
            $('#edit-country options:first').attr('disabled', 'disabled');

            $('select', context).formSelect();

            let field = settings['field'];
            // [\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}

            if(field !== undefined){
                var speed = 1000;// скорость скролла
                var top = $('#top-charester').offset().top;  // место скролла
                $('html, body',context).animate({scrollTop: top}, speed);
                return false;
            }
        }
    };
})(jQuery);