<?php
/**
 * theme file
 */

?>

<section>
    <h2 style="text-align:center;color:#205897;" class="terrasoft-head">
      <?php print render($form['header']); ?>
    </h2>
    <a id="top-charester" style="display: none" href="top"></a>
    <div id="form-background">
        <p style="text-align:center;" class="terrasoft-head">
          <?php print render($form['title']); ?>
        </p>
        <div class="row">
            <div class="col s7 m7 padding-no-left push-s5">
                <div class="input-field inline domain" style="flex:2;">
                  <?php print render($form['domain']); ?>
                    <span class="helper-text" data-error="wrong"
                          data-success="right">Help text</span>
                </div>
            </div>
            <div class="domain-text col s5 m5 pull-s7">
                <span>
                    .bpmonline.com
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="input-field login">
                  <?php print render($form['login']); ?>
                    <span class="helper-text" data-error="wrong"
                          data-success="right">Help text</span>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col s12">

                <div class="input-field country">
                  <?php print render($form['country']); ?>
                    <span class="helper-text" data-error="wrong"
                          data-success="right">Help text</span>
                </div>

            </div>
        </div>
        <div class="row">

            <div class="col s12">

                <div class="input-field city">
                  <?php print render($form['city']); ?>
                    <span class="helper-text" data-error="wrong"
                          data-success="right">Help text</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s4 m4 code">
              <?php print render($form['code']); ?>
                <span class="helper-text" data-error="wrong"
                      data-success="right">Help text</span>
            </div>
            <div class="input-field col s8 m8 phone">
              <?php print render($form['phone']); ?>
                <span class="helper-text" data-error="wrong"
                      data-success="right">Help text</span>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 submit-button">
              <?php print render($form['submit']); ?>
            </div>
        </div>
        <div class="center-align terrasoft-text white-text">
          <?php print render($form['create_account']); ?>
        </div>
    </div>
    <center class="terrasoft-text bottom-menu">
      <?php print render($form['menu_bottom']); ?>
    </center>
</section>
<?php print drupal_render_children($form); ?>
